﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public class EmployeeMapper
        : IEmployeeMapper
    {
        private readonly IRepository<Role> _rolesRepository;

        public EmployeeMapper(IRepository<Role> rolesRepository)
        {
            _rolesRepository = rolesRepository;
        }
        
        public async Task<Employee> MapFromModelAsync(CreateOrEditEmployeeRequest model, Employee employee = null)
        {
            //Здесь можно использовать Automapper, если будем исползовать этот класс, то не будем напрямую привязаны
            //к Automapper во всем коде
            
            if (employee == null)
            {
                employee = new Employee();
                employee.Id = Guid.NewGuid();
            }
            
            //Логика нужная для создания/маппинга объекта, можно положить в сюда
            var roles  = await _rolesRepository.GetByCondition(x => 
                ((IList) model.RoleNames).Contains(x.Name)) as List<Role>;
            
            employee.FirstName = model.FirstName;
            employee.LastName = model.LastName;
            employee.AppliedPromocodesCount = model.AppliedPromocodesCount;
            employee.Email = model.Email;
            employee.Roles = roles;

            return employee;
        }
    }
}