﻿using System;

namespace Otus.Teaching.PromoCodeFactory.ConsoleHost.Dto
{
    public class EmployeeShortInfoDto
    {
        public Guid Id { get; set; }
        
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}